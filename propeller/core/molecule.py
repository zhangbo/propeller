#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Bo Zhang

import collections
import copy
import functools
import propeller.core.element as element
import numpy as np

class Molecule:
    """
        molecule class
        member:
            __atomic_positions_cartesian: atomic positions in cartesian coordinate; numpy 2d array, element will be [x, y, z, atomic number], to be used with numpy, first three will be cartesian coordinate, last one is atomic number
            __atomic_positions_fractional: atomic positions in fractional coordinate; numpy 2d array, element will be [x, y, z, atomic number], to be used with numpy, first three will be cartesian coordinate, last one is atomic number
            __charges: charges of atoms; list of float
            __connectivity: connectivity list of each atom, each connectivity is a list of atom indexes; list of list of int
            __fragments: indexes of atoms are connected together (a group of bonded atoms) of this molecule; list of list of int
            __name: name of the molecule; str
            __reciprocalcell: unitcell of this molecule; list of list
            __unitcell: unitcell of this molecule; list of list
    """

    def __init__(self, atomic_positions_cartesian=[], atomic_positions_fractional=[], unitcell=[]):
        """
            initialization
            arguments:
                atomic_positions_cartesian: atomic positions in cartesian coordinate; list, defaults to empty list
                atomic_positions_fractional: atomic positions in fractional coordinate; list, defaults to empty list
                unitcell: unitcell; list, defaults to empty list
        """
        self.__atomic_positions_cartesian = np.array(atomic_positions_cartesian)
        self.__atomic_positions_fractional = np.array(atomic_positions_fractional)
        self.__charges = []
        self.__connectivity = []
        self.__fragments = []
        self.__name = ''
        self.__unitcell = unitcell 
        if len(unitcell) == 3:
            self.__reciprocalcell = np.linalg.inv(unitcell)

    def __str__(self):
        """ print information """
        print(self.name)
        if len(self.unitcell) == 3:
            print(self.unitcell)
        if len(self.atomic_positions_cartesian) > 0:
            print(self.atomic_positions_cartesian)
        if len(self.atomic_positions_fractional) > 0:
            print(self.atomic_positions_fractional)
        if len(self.charges) > 0:
            print(self.charges)

    @functools.lru_cache(maxsize=None)
    def angle(self, index1, index2, index3):
        """
            calculate angle for specified three atom indexes
            arguments:
                index1: atom index; int
                index2: atom index, vertex of the angle; int
                index3: atom index; int
            returns:
                angle; float
        """
        # non-periodic
        if len(self.unitcell) == 0:
            vector1 = self.atomic_positions_cartesian[index1][:3] - self.atomic_positions_cartesian[index2][:3]
            vector2 = self.atomic_positions_cartesian[index3][:3] - self.atomic_positions_cartesian[index2][:3]
        # periodic
        elif len(self.unitcell) == 3:
            vector1 = self.atomic_positions_fractional[index1][:3] - self.atomic_positions_fractional[index2][:3]
            vector2 = self.atomic_positions_fractional[index3][:3] - self.atomic_positions_fractional[index2][:3]
            apply_minimum_image_convention(vector1)
            apply_minimum_image_convention(vector2)
            vector1 = np.dot(vector1, self.unitcell)
            vector2 = np.dot(vector2, self.unitcell)
        else:
            print(self.unitcell)
            raise ValueError('the unitcell is NOT correct')
        return np.arccos( np.dot(vector1, vector2) / np.linalg.norm(vector1) / np.linalg.norm(vector2) ) / np.pi * 180

    @property
    def atomic_positions_cartesian(self):
        return self.__atomic_positions_cartesian

    @atomic_positions_cartesian.setter
    def atomic_positions_cartesian(self, value):
        if isinstance(value, list):
            self.__atomic_positions_cartesian = np.array(value)
        elif isinstance(value, np.ndarray):
            self.__atomic_positions_cartesian = value
        else:
            raise TypeError('unrecognized value')
    
    @property
    def atomic_positions_fractional(self):
        return self.__atomic_positions_fractional

    @atomic_positions_fractional.setter
    def atomic_positions_fractional(self, value):
        if isinstance(value, list):
            self.__atomic_positions_fractional = np.array(value)
        elif isinstance(value, np.ndarray):
            self.__atomic_positions_fractional = value
        else:
            raise TypeError('unrecognized value')

    def calculate_connectivity(self, bonding_criteria='bv_table_and_covalent', bonding_tolerance=0.2):
        """
            calculate connectivity for each atom by specified bonding criteria and tolerance
            arguments:
                bonding_criteria: the method to get the bond length of two atoms; str, can only be 'bond_valence_table', 'bv_table_and_covalent', 'covalent_radii', defaults to 'bv_table_and_covalent'
                bonding_tolerance: the allowed difference between the distance of two atoms and the bond length from literature, defaults to 0.2
        """
        # create a dictionary of bond types to save time
        bond_types = {}
        for i in self.unique_elements:
            for j in self.unique_elements:
                bond_types[(i, j)] = element.get_bond_length(i, j, bonding_criteria = bonding_criteria)

        symbols = self.element_symbols
        self.__connectivity = [[] for i in range(self.number_of_atoms)]
        for i in range(self.number_of_atoms):
            for j in range(i + 1, self.number_of_atoms):
                for b in bond_types[(symbols[i], symbols[j])]:
                    if np.fabs(self.distance(i, j) - b) < bonding_tolerance and self.distance(i, j) > 0.5:
                        self.__connectivity[i].append(j)
                        self.__connectivity[j].append(i)
                        break

    def cartesian_to_fractional(self):
        """ convert cartesian coordinate to fractional coordinate """
        if len(self.unitcell) == 3 and len(self.atomic_positions_cartesian) > 0:
            self.atomic_positions_fractional = np.column_stack( (np.dot(self.atomic_positions_cartesian[:,:3], self.reciprocalcell), self.atomic_positions_cartesian[:,3]) )

    @property
    def center_of_geometry(self):
        return calculate_center_of_property(self.atomic_positions_cartesian, 'geometry')

    @property
    def center_of_mass(self):
        return calculate_center_of_property(self.atomic_positions_cartesian, 'mass')

    @property
    def center_of_valence_electron(self):
        return calculate_center_of_property(self.atomic_positions_cartesian, 'valence_electron')

    @property
    def charges(self):
        return self.__charges

    @charges.setter
    def charges(self, value):
        self.__charges = value

    def connect(self, bonding_criteria='bv_table_and_covalent', bonding_tolerance=0.2):
        """
            calculate bonds of the molecule based on certain criteria within given tolerance, also translate atoms if necessary to make it look nicer (i. e. similar as default display style in Material's Studio), for non periodic system, only calculate connectivity
            arguments:
                bonding_criteria: the method to get the bond length of two atoms; str, can only be 'bond_valence_table', 'bv_table_and_covalent', 'covalent_radii', defaults to 'bv_table_and_covalent'
                bonding_tolerance: tolerance to determine if a bond forming; float, defaults to 0.2
        """
        self.calculate_connectivity(bonding_criteria = bonding_criteria, bonding_tolerance = bonding_tolerance)
        self.set_fragments_by_connectivity()
        # only connect fragments for periodic system
        if len(self.unitcell) == 3:
            self.connect_fragments()

    def connect_fragments(self):
        """ connect atoms within one fragment by applying minimum image convention for every fragment """
        visited = []
        for f in self.fragments:
            for index in f:
                visited.append(index)
                for j in self.connectivity[index]:
                    if j not in visited:
                        for k in range(3):
                            while self.atomic_positions_fractional[j][k] - self.atomic_positions_fractional[index][k] > 0.5:
                                self.atomic_positions_fractional[j][k] -= 1
                            while self.atomic_positions_fractional[j][k] - self.atomic_positions_fractional[index][k] < -0.5:
                                self.atomic_positions_fractional[j][k] += 1

            com = calculate_center_of_property(np.array([self.atomic_positions_fractional[i] for i in f]), 'mass')

            for index in f:
                for i in range(3):
                    if com[i] > 1:
                        self.atomic_positions_fractional[index][i] -= 1
                    if com[i] < 0:
                        self.atomic_positions_fractional[index][i] += 1
        self.fractional_to_cartesian()

    @property
    def connectivity(self):
        return self.__connectivity

    def copy(self, mol):
        """
            copy __atomic_positions_cartesian, __atomic_positions_fractional, __charges, __connectivity, __fragments, __unitcell from input molecule object
            arguments:
                mol: input molecule; Molecule class
        """
        self.__atomic_positions_cartesian = copy.deepcopy(mol.atomic_positions_cartesian)
        self.__atomic_positions_fractional = copy.deepcopy(mol.atomic_positions_fractional)
        self.__charges = copy.deepcopy(mol.charges)
        self.__connectivity = copy.deepcopy(mol.connectivity)
        self.__fragments = copy.deepcopy(mol.fragments)
        self.__name = copy.deepcopy(mol.name)
        self.__unitcell = copy.deepcopy(mol.unitcell)
        self.__reciprocalcell = np.linalg.inv(self.__unitcell)
        
    @functools.lru_cache(maxsize=None)
    def dihedral_angle(self, index1, index2, index3, index4):
        """
            calculate dihedral angle between the four specified atoms
            arguments:
                index1: atom index; int
                index2: atom index; int
                index3: atom index; int
                index4: atom index; int
                index1 and index4 will be the end atoms
            returns:
                dihedral angle; float
        """
        # non-periodic
        if len(self.unitcell) == 0:
            vector1 = self.atomic_positions_cartesian[index2][:3] - self.atomic_positions_cartesian[index1][:3]
            vector2 = self.atomic_positions_cartesian[index3][:3] - self.atomic_positions_cartesian[index2][:3]
            vector3 = self.atomic_positions_cartesian[index4][:3] - self.atomic_positions_cartesian[index3][:3]
        # periodic
        elif len(self.unitcell) == 3:
            vector1 = self.atomic_positions_fractional[index2][:3] - self.atomic_positions_fractional[index1][:3]
            vector2 = self.atomic_positions_fractional[index3][:3] - self.atomic_positions_fractional[index2][:3]
            vector3 = self.atomic_positions_fractional[index4][:3] - self.atomic_positions_fractional[index3][:3]
            apply_minimum_image_convention(vector1)
            apply_minimum_image_convention(vector2)
            apply_minimum_image_convention(vector3)
            vector1 = np.dot(vector1, self.unitcell)
            vector2 = np.dot(vector2, self.unitcell)
            vector3 = np.dot(vector3, self.unitcell)
        else:
            raise ValueError('the unitcell is NOT correct')
        return np.arctan2(np.linalg.norm(vector2) * np.dot(vector1, np.cross(vector2, vector3)), np.dot(np.cross(vector1, vector2), np.cross(vector2, vector3))) / np.pi * 180

    def displace_atom(self, index, axis, delta=0.01):
        """
            displace a single atom
            arguments:
                index: atom index to be displaced; int
                axis: x, y, z direction; int
                delta: displacement; float, defaults to 0.01
        """
        self.atomic_positions_cartesian[index][axis] += delta
        if len(self.unitcell) == 3:
            self.cartesian_to_fractional()

    @functools.lru_cache(maxsize=None)
    def distance(self, index1, index2):
        """
            calculate distance between two specified atoms
            arguments:
                index1: atom index; int
                index2: atom index; int
            returns:
                distance; float
        """
        return np.linalg.norm(self.distance_vector(index1, index2, 'cartesian'))

    def distance_vector(self, index1, index2, coordinate='cartesian'):
        """
            calculate distance vector between two specified atoms either in cartesian coordinate or fractional coordinate
            arguments:
                index1: atom index; int
                index2: atom index; int
                coordinate: coordinate; can only be 'cartesian' or 'fractional'
            returns:
                distance_vector; list
        """
        # non-periodic
        if len(self.unitcell) == 0:
            if coordinate == 'cartesian':
                return self.atomic_positions_cartesian[index1][:3] - self.atomic_positions_cartesian[index2][:3]
            elif coordinate == 'fractional':
                raise ValueError('non periodic system have no fractional coordinate')
            else:
                raise ValueError('No such coordinate {0:s}'.format(coordinate))
        # periodic
        elif len(self.unitcell) == 3:
            if len(self.atomic_positions_fractional) > 0:
                distance_vector = self.atomic_positions_fractional[index1][:3] - self.atomic_positions_fractional[index2][:3]
            elif len(self.atomic_positions_cartesian) > 0:
                distance_vector = np.dot(self.atomic_positions_cartesian[index1][:3] - self.atomic_positions_cartesian[index2][:3], self.__reciprocalcell) 
            apply_minimum_image_convention(distance_vector)
            if coordinate == 'cartesian':
                return np.dot(distance_vector, self.unitcell)
            elif coordinate == 'fractional':
                return distance_vector
            else:
                raise ValueError('No such coordinate {0:s}'.format(coordinate))
        else:
            raise ValueError('the unitcell is NOT correct')

    @property
    def element_symbols(self):
        """ element symbols """
        if len(self.atomic_positions_cartesian) > 0:
            atomic_numbers = self.atomic_positions_cartesian[:,3]
        elif len(self.atomic_positions_fractional) > 0:
            atomic_numbers = self.atomic_positions_fractional[:,3]
        else:
            return []
        return [element.get_element_symbol(i) for i in atomic_numbers]

    def find_all_bond_angle_dihedral(self, is_considering_improper_dihedral=False):
        """
            find all bonds, angles, dihedral angles, and improper dihedral angles
            arguments:
                is_considering_improper_dihedral: find improper dihedral angles or not; bool, defaults to False
            returns:
                bonds: bonded atom indexes; list of list, element will be [index1, index2]
                angles: angle atom indexes; list of list, element will be [index1, index2, index3], index2 is the vectex atom of the angle
                dihedral_angles: dihedral angles atom indexes; list of list, element will be [index1, index2, index3, index4], index1 and index4 will be the end atoms
                improper_dihedral_angles: dihedral angles atom indexes; list of list, element will be [index1, index2, index3, index4], index1 is the atom connects to the other three atoms, i. e. the vectex of the improper dihedral angle
        """
        if not self.connectivity:
            raise SyntaxError('calculate connectivity first')

        bonds = []
        angles = []
        dihedral_angles = []
        improper_dihedral_angles = []
        # find bonds
        for index in range(len(self.connectivity)):
            for j in self.connectivity[index]:
                if sorted([index, j]) not in bonds:
                    bonds.append( sorted([index, j]) )

        # find angles
        for index in range(len(self.connectivity)):
            for j in self.connectivity[index]:
                for k in self.connectivity[index]:
                    if j != k and [j, index, k] not in angles and [k, index, j] not in angles:
                        angles.append( [j, index, k] )

        # find dihedral angles
        for b in bonds:
            for j in self.connectivity[b[0]]:
                for k in self.connectivity[b[1]]:
                    if j != k and j != b[1] and k != b[0] and [j, b[0], b[1], k] not in dihedral_angles and [k, b[1], b[0], j] not in dihedral_angles:
                        dihedral_angles.append( [j, b[0], b[1], k] )

        if is_considering_improper_dihedral:
            for index in range(len(self.connectivity)):
                if len(self.connectivity[index]) == 3:
                    improper_dihedral_angles.append([index] + sorted(self.connectivity[index]))
                elif len(self.connectivity[index]) == 4:
                    for j in range(4):
                        improper_dihedral_angles.append([index] + sorted([self.connectivity[index][j % 4], self.connectivity[index][(j + 1) % 4], self.connectivity[index][(j + 2) % 4]]))
        return bonds, angles, dihedral_angles, improper_dihedral_angles

    def find_locant(self, index, atom_zero, fragment=[]):
        """
            find locant for the atom relative to atom_zero within the fragment or molecule (fragment is empty)
            arguments:
                index: atom index; int
                atom_zero: names or indexes of atoms with label zero; str or int, if the input is name of atom, the atom should be unique element in the fragment, it can be a list of atoms with the same element symbol
                fragment: fragmnet; list of int
            returns:
                locant: locant; int
        """
        if len(fragment) == 0:
            fragment = range(self.number_of_atoms)
        queue = collections.deque()
        zero_index = []
        if isinstance(atom_zero, int):
            zero_index.append(atom_zero)
        elif isinstance(atom_zero, str):
            for i in fragment:
                if self.element_symbols[i] == atom_zero:
                    zero_index.append(i)
        elif isinstance(atom_zero, list):
            if all(isinstance(i, int) for i in atom_zero):
                zero_index = atom_zero
            else:
                raise TypeError('the type of atom_zero is NOT supported')
        else:
            raise TypeError('the type of atom_zero is NOT supported')
        if index in zero_index:
            return 0
        elif self.element_symbols[index] != 'H':
            for i in self.connectivity[index]:
                queue.append([i, 1])
        elif self.element_symbols[index] == 'H' and len(self.connectivity[index]) == 1:
            # for H, use attached heavy atom index 
            if self.connectivity[index][0] in zero_index:
                return 0
            for i in self.connectivity[self.connectivity[index][0]]:
                queue.append([i, 1])
        else:
            raise ValueError('H attached to more than one atoms')
        # breadth first search
        while len(queue) > 0:
            current = queue.popleft()
            if current[0] in zero_index:
                return current[1]
            elif self.element_symbols[current[0]] != 'H':
                for i in self.connectivity[current[0]]:
                    if self.element_symbols[i] != 'H':
                        queue.append([i, current[1] + 1])

    def find_primary_atom_index(self, fragment=[]):
        """
            find primary atom index for specified indexes, if fragment is empty, return the primary atom index for the molecule
            arguments:
                fragment: fragment atom indexes; list of int
            returns:
                primary_atom_index: 
        """
        if len(fragment) == 0:
            fragment = range(self.number_of_atoms)
        # exclude H and C
        heavy_atom_indexes = list(filter(lambda x: self.element_symbols[x] != 'H' and self.element_symbols[x] != 'C', fragment))
        if len(heavy_atom_indexes) == 0:
            # consider C
            heavy_atom_indexes = list(filter(lambda x: self.element_symbols[x] != 'H', fragment))
        if len(heavy_atom_indexes) == 1:
            return heavy_atom_indexes[0]
        depthes = []
        for i in heavy_atom_indexes:
            if len(self.connectivity[i]) == 1:
                depthes.append(0)
                continue
            else:
                queue = collections.deque()
                for j in self.connectivity[i]:
                        queue.append([j, 1])
                while len(queue) > 0:
                    current = queue.popleft()
                    if len(self.connectivity[current[0]]) == 1:
                        depthes.append(current[1])
                        break
                    else:
                        for j in self.connectivity[current[0]]:
                            queue.append([j, current[1] + 1])
        # no matter how many heavy atom have maximum depth, we only need one
        return heavy_atom_indexes[depthes.index(max(depthes))]
        
    def formula(self, fragment=[]):
        """
            get chemical formula of the atoms with specified indexes, if fragment is empty, return chemical formula of the all atoms of the molecule, i. e. the chemical formula of the molecule
            arguments:
                fragment: fragment atom indexes; list of int
            returns:
                formula: chemical formula; str
        """
        if len(fragment) == 0:
            fragment = range(self.number_of_atoms)
        c = collections.Counter([self.element_symbols[i] for i in fragment])
        return ''.join([x + str(y) for x, y in collections.OrderedDict(sorted(c.items(), key=lambda x: x[0])).items()])

    def focus(self, atom_index, is_atoms_in_cell=True):
        """
            focus on specified atom by given index, i. e. move this atom to box center and move other atoms accordingly
            arguments:
                atom_index: focused atom index; int
                is_atoms_in_cell: if atoms will be in cell; bool, defaults to True
        """
        if len(self.unitcell) == 0:
            raise ValueError('Not a periodic system')
        distance_vector = self.atomic_positions_fractional[atom_index][:3] - np.array([0.5, 0.5, 0.5])
        for atom in self.atomic_positions_fractional[:,:3]:
            atom -= distance_vector
            if is_atoms_in_cell:
                normalize(atom)
        self.fractional_to_cartesian
    
    def fractional_to_cartesian(self):
        """ convert fractional coordinate to cartesian coordinate """
        if len(self.unitcell) == 3 and len(self.atomic_positions_fractional) > 0:
            self.atomic_positions_cartesian = np.column_stack( (np.dot(self.atomic_positions_fractional[:,:3], self.unitcell), self.atomic_positions_fractional[:,3]) )
 
    @property
    def fragments(self):
        return self.__fragments

    @fragments.setter
    def fragments(self, value):
        self.__fragments = value

    @functools.lru_cache(maxsize=None) 
    def improper_dihedral_angle(self, index1, index2, index3, index4):
        """
            calculate dihedral angle between the four specified atoms
            arguments:
                index1: atom index, this is the vectex of this improper dihedral angle; int
                index2: atom index; int
                index3: atom index; int
                index4: atom index; int
            returns:
                dihedral angle; float
        """
        # non-periodic
        if len(self.unitcell) == 0:
            vector1 = self.atomic_positions_cartesian[index2][:3] - self.atomic_positions_cartesian[index1][:3]
            vector2 = self.atomic_positions_cartesian[index3][:3] - self.atomic_positions_cartesian[index1][:3]
            vector3 = self.atomic_positions_cartesian[index4][:3] - self.atomic_positions_cartesian[index1][:3]
        # periodic
        elif len(self.unitcell) == 3:
            vector1 = self.atomic_positions_fractional[index2][:3] - self.atomic_positions_fractional[index1][:3]
            vector2 = self.atomic_positions_fractional[index3][:3] - self.atomic_positions_fractional[index1][:3]
            vector3 = self.atomic_positions_fractional[index4][:3] - self.atomic_positions_fractional[index1][:3]
            apply_minimum_image_convention(vector1)
            apply_minimum_image_convention(vector2)
            apply_minimum_image_convention(vector3)
            vector1 = np.dot(vector1, self.unitcell)
            vector2 = np.dot(vector2, self.unitcell)
            vector3 = np.dot(vector3, self.unitcell)
        else:
            raise ValueError('the unitcell is NOT correct')
        return np.arctan2(np.linalg.norm(vector2) * np.dot(vector1, np.cross(vector2, vector3)), np.dot(np.cross(vector1, vector2), np.cross(vector2, vector3))) / np.pi * 180

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__name = value

    @property
    def number_of_atoms(self):
        """ number of atoms in the molecule """
        if len(self.atomic_positions_cartesian) > 0:
            return self.atomic_positions_cartesian.shape[0]
        elif len(self.atomic_positions_fractional) > 0:
            return self.atomic_positions_fractional.shape[0]
        else:
            return 0
    
    @property
    def reciprocalcell(self):
        return self.__reciprocalcell

    @reciprocalcell.setter
    def reciprocalcell(self, value):
        self.__reciprocalcell = value
        self.__unitcell = np.linalg.inv(self.__reciprocalcell)

    def set_fragments_by_connectivity(self):
        """ set fragments by finding group of atoms that are connected with each other via bonds, split them into several fragments """
        for index in range(self.number_of_atoms):
            if index not in (i for j in self.fragments for i in j):
                stack = [index]
                fragment = []
                # depth-first
                while len(stack) > 0:
                    visited = stack.pop()
                    if visited not in fragment:
                        fragment.append(visited)
                        stack += self.connectivity[visited]
                self.fragments.append(fragment)

    def translate_atoms_in_cell(self):
        """ translate atoms in cell for periodic system """
        for a in self.atomic_positions_fractional:
            normalize(a)
        self.fractional_to_cartesian()

    @property
    def unitcell(self):
        return self.__unitcell

    @unitcell.setter
    def unitcell(self, value):
        self.__unitcell = value
        self.__reciprocalcell = np.linalg.inv(self.__unitcell)

    @property
    def unique_elements(self):
        """ unique elements of the molecule """
        return sorted(collections.Counter(self.element_symbols))

    @property
    def unique_elements_counter(self):
        """ number of atoms of each unique element """
        return [collections.Counter(self.element_symbols)[i] for i in self.unique_elements]

# utility functions
def apply_minimum_image_convention(position):
    """ apply minimum image convention to the atomic positions to set -0.5 <= x, y, z <= 0.5 for fractional coordinate """
    for i in range(3):
        while position[i] < -0.5:
            position[i] += 1
        while position[i] > 0.5:
            position[i] -= 1

def calculate_center_of_property(atoms, property_name='geometry'):
    """
        calculate center of property of a group of atoms
        arguments:
            atoms: atom positions and atomic numbers; numpy 2d array
            property_name: property to be calculated; str, can only be 'geometry', 'mass', 'valence_electron', defaults to geometry
        returns:
            center of the property; float
    """
    if property_name == 'geometry':
        properties = [1 for i in range(len(atoms))]
    elif property_name == 'mass':
        properties = [element.get_mass(i[3]) for i in atoms]
    elif property_name == 'valence_electron':
        properties = [element.get_number_of_valence_electron(i[3]) for i in atoms]
    return np.dot(properties, atoms[:,:3]) / sum(properties)

def normalize(position):
    """ normalize the atomic positions to be 0 <= x, y, z <= 1 for fractional coordinate """
    for i in range(3):
        while position[i] < 0:
            position[i] += 1
        while position[i] > 1:
            position[i] -= 1
