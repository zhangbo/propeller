#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Bo Zhang

"""
    some useful constants, taken from http://www.gaussian.com/g_tech/g_ur/k_constants.htm
"""
AVOGADRO_NUMBER = 6.02214179e23 # Avogadro's number
BOLTZMAN_CONSTANT = 1.3806504e-23 # Boltman constant, J/K
ELEMENTARY_CHARGE = 1.602176487e-19 # Elementary charge, C
MASS_OF_ELECTRON = 9.1093897e-31 # Mass of electron, kg
PLANCK_CONSTANT = 6.62606896e-34 # Planck constant, J*s
SPEED_OF_LIGHT = 299792458. # Speed of light, m/s

"""
    some useful units convertion factor
"""
AMU_TO_KG = 1.66053878e-27 # Atomic mass unit to kg, kg
ANGSTROM_TO_M = 1e-10 # Angstrom to m, m
CAL_TO_J = 4.184 # Calorie to Joule, J
E_ANGSTROM_TO_DEBYE = 1e21 * SPEED_OF_LIGHT * ELEMENTARY_CHARGE * ANGSTROM_TO_M # e*angstrom to debye
EV_TO_J = 1.60217649e-19 # eV to Joule, J
HARTREE_TO_J = 4.3597439e-18 # eV to Joule, J

# shortcuts
ANGSTROM = ANGSTROM_TO_M
AMU = AMU_TO_KG
C = SPEED_OF_LIGHT
CAL = CAL_TO_J
E = ELEMENTARY_CHARGE
EA_TO_D = E_ANGSTROM_TO_DEBYE
EV = EV_TO_J
H = PLANCK_CONSTANT
HARTREE = HARTREE_TO_J
KCAL = 1000 * CAL_TO_J
KB = BOLTZMAN_CONSTANT
KJ = 1000
ME = MASS_OF_ELECTRON
NA = AVOGADRO_NUMBER

""" unit str """
STR_ANGSTROM = '\u00c5'
STR_CAL = 'Cal'
STR_DEBYE = 'D'
STR_EV = 'eV'
STR_J = 'J'
STR_KCAL = 'kCal'
STR_KJ = 'kJ'

