# -*- coding: utf-8 -*-
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Bo Zhang

import numpy as np
import propeller.core.element as element
import propeller.core.molecule as molecule

class Xyz(molecule.Molecule):
    """
        xyz file, only one molecule per file
    """
    def __init__(self):
        pass

    def read(self, filename):
        """
            read in xyz file, only one molecule per file
            arguments:
                filename: file name; str
        """
        try:
            filein = open(filename, 'r', encoding='utf-8')
        except IOError:
            raise
        print('reading ' + filename + ' ... ', end='')
        n_atoms = int(filein.readline())
        self.name = filein.readline().strip()
        content = [filein.readline() for i in range(n_atoms)]
        self.atomic_positions_cartesian = np.array([[float(j) for j in i.split()[1:4]] + [element.get_atomic_number(i.split()[0])] for i in content])
        # possible charge
        if len(content[0].split()) == 5:
            self.charges = np.array([float(i.split()[4]) for i in content])
        filein.close()
        print('done')

    def write(self, filename, is_writing_charge=False):
        """
            write xyz file, one molecule per file
            arguments:
                filename: file name; str
                is_writing_charge: if write charges; bool
        """
        try:
            fileout = open(filename, 'w', encoding='utf-8')
        except IOError:
            raise
        print('writing ' + filename + ' ... ', end='')
        fileout.write(str(self.number_of_atoms) + '\n')
        fileout.write(self.name + '\n')
        for i in range(self.number_of_atoms):
            fileout.write(element.get_element_symbol(self.atomic_positions_cartesian[i][3]))
            fileout.write(' {0:.6f}  {1:.6f}  {2:.6f}'.format(self.atomic_positions_cartesian[i][0], self.atomic_positions_cartesian[i][1], self.atomic_positions_cartesian[i][2]))
            if is_writing_charge and len(self.charges) == self.number_of_atoms:
                fileout.write(' {0:4f}'.format(self.charges[i]))
            fileout.write('\n')
        fileout.close()
        print('done')
       
class Xyzs:
    """
        general xyz file, may contain more than one molecules
        members:
            __molecules: molecules; list of Molecule
    """
    def __init__(self):
        """ initialization """
        self.__molecules = []
        
    @property
    def molecules(self):
        return self.__molecules
    
    @molecules.setter
    def molecules(self, value):
        self.__molecules = value

    @property
    def number_of_molecules(self):
        return len(self.molecules)

    def read(self, filename):
        """
            read in xyz file
            arguments:
                filename: file name; str
        """
        try:
            filein = open(filename, 'r', encoding='utf-8')
        except IOError:
            raise
        print('reading ' + filename + ' ... ', end='')
        line = filein.readline()
        while filein:
            n_atoms = int(line)
            mol = molecule.Molecule()
            mol.name = filein.readline().strip()
            content = [filein.readline() for i in range(n_atoms)]
            mol.atomic_positions_cartesian = np.array([[float(j) for j in i.split()[1:4]] + [element.get_atomic_number(i.split()[0])] for i in content])
            # possible charge
            if len(content[0].split()) == 5:
                mol.charges = np.array([float(i.split()[4]) for i in content])
            self.molecules.append(mol)
            line = filein.readline()
            if len(line.strip()) == 0:
                break
        filein.close()
        print('done')

    def write(self, filename, is_writing_charge=False):
        """
            write xyz file
            arguments:
                filename: file name; str
                is_writing_charge: if write charges; bool
        """
        try:
            fileout = open(filename, 'w', encoding='utf-8')
        except IOError:
            raise
        print('writing ' + filename + ' ... ', end='')
        for i in self.molecules:
            fileout.write(str(i.number_of_atoms) + '\n')
            fileout.write(i.name + '\n')
            for j in range(i.number_of_atoms):
                fileout.write(element.get_element_symbol(i.atomic_positions_cartesian[j][3]))
                fileout.write(' {0:.6f}  {1:.6f}  {2:.6f}'.format(i.atomic_positions_cartesian[j][0], i.atomic_positions_cartesian[j][1], i.atomic_positions_cartesian[j][2]))
                if is_writing_charge and len(i.charges) == i.number_of_atoms:
                    fileout.write(' {0:4f}'.format(i.charges[j]))
                fileout.write('\n')
        fileout.close()
        print('done')

            
