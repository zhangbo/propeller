#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Bo Zhang

import copy
import numpy as np
import propeller.core.element as element
import propeller.core.molecule as molecule

class Data(molecule.Molecule):
    """
        lammps input data file
        members:
            __angle_coefficients: all angle coefficients; list of list
            __angles: all angles of the molecule, the first item must be its type; list of list, the central atom is the second atom
            __atom_types: atom type of each atom, start from 1; list with length of self.number_of_atoms
            __bond_coefficients: all bond coefficients; list of list
            __bonds: all bonds of the molecule, the first item must be its type; list of list 
            __dihedral_angle_coefficients: all dihedral angle coefficients; list of list
            __dihedral_angles: all dihedral angles of the molecule, the first item must be its type; list of list
            __improper_dihedral_angle_coefficients: all improper dihedral angle coefficients; list of list
            __improper_dihedral_angles: all improper dihedral angles of the molecule, the first item must be its type; list of list, the central atom is the first atom
            __mol_ids: molecular ids, starts from 1; list
            __pair_coefficients: all pair coefficients; list of list
            __type_element_symbols: element symbols corresponding to atom type, Note this is NOT complete element symbols; list
    """

    def __init__(self):
        """ initialization """
        molecule.Molecule.__init__(self)
        self.__angle_coefficients = []
        self.__angles = []
        self.__atom_types = []
        self.__bond_coefficients = []
        self.__bonds = []
        self.__dihedral_angle_coefficients = []
        self.__dihedral_angles = []
        self.__type_element_symbols = []
        self.__improper_dihedral_angle_coefficients = []
        self.__improper_dihedral_angles = []
        self.__mol_ids = []
        self.__pair_coefficients = []

    @property
    def angle_coefficients(self):
        return self.__angle_coefficients

    @angle_coefficients.setter
    def angle_coefficients(self, value):
        self.__angle_coefficients = value

    @property
    def angles(self):
        return self.__angles

    @angles.setter
    def angles(self, value):
        self.__angles = value

    @property
    def atom_types(self):
        return self.__atom_types

    @atom_types.setter
    def atom_types(self, value):
        self.__atom_types = value

    @property
    def bond_coefficients(self):
        return self.__bond_coefficients

    @bond_coefficients.setter
    def bond_coefficients(self, value):
        self.__bond_coefficients = value

    @property
    def bonds(self):
        return self.__bonds

    @bonds.setter
    def bonds(self, value):
        self.__bonds = value

    @property
    def dihedral_angle_coefficients(self):
        return self.__dihedral_angle_coefficients

    @dihedral_angle_coefficients.setter
    def dihedral_angle_coefficients(self, value):
        self.__dihedral_angle_coefficients = value

    @property
    def dihedral_angles(self):
        return self.__dihedral_angles

    @dihedral_angles.setter
    def dihedral_angles(self, value):
        self.__dihedral_angles = value

    @property
    def improper_dihedral_angle_coefficients(self):
        return self.__improper_dihedral_angle_coefficients

    @improper_dihedral_angle_coefficients.setter
    def improper_dihedral_angle_coefficients(self, value):
        self.__improper_dihedral_angle_coefficients = value

    @property
    def improper_dihedral_angles(self):
        return self.__improper_dihedral_angles

    @improper_dihedral_angles.setter
    def improper_dihedral_angles(self, value):
        self.__improper_dihedral_angles = value

    @property
    def mol_ids(self):
        return self.__mol_ids

    @mol_ids.setter
    def mol_ids(self, value):
        self.__mol_ids = value
    
    @property
    def pair_coefficients(self):
        return self.__pair_coefficients

    @pair_coefficients.setter
    def pair_coefficients(self, value):
        self.__pair_coefficients = value

    def read(self, filename, data_format='full'):
        """
            read lammps input data
            arguments:
                filename: input file name; str
                data_format: lammps data format; can be 'atomic', 'charge', 'full', defaults to 'full'
        """
        try:
            filein = open(filename, 'r', encoding='utf-8')
        except IOError:
            raise
        print('reading ' + filename + ' ... ', end='')
        file_content = filein.readlines()
        filein.close()
        unitcell = np.zeros([3, 3])
        for i, line in enumerate(file_content):
            if i == 0:
                self.name = file_content[0].strip()
            elif 'atoms' in line:
                n_atoms = int(line.split()[0])
            elif 'bonds' in line:
                n_bonds = int(line.split()[0])
            elif 'angles' in line:
                n_angles = int(line.split()[0])
            elif 'dihedrals' in line:
                n_dihedrals = int(line.split()[0])
            elif 'impropers' in line:
                n_impropers = int(line.split()[0])
            elif 'atom types' in line:
                n_atom_types = int(line.split()[0])
            elif 'bond types' in line:
                n_bond_types = int(line.split()[0])
            elif 'angle types' in line:
                n_angle_types = int(line.split()[0])
            elif 'dihedral types' in line:
                n_dihedral_types = int(line.split()[0])
            elif 'improper types' in line:
                n_improper_types = int(line.split()[0])
            elif 'xlo' in line and 'xhi' in line:
                unitcell[0, 0] = float(line.split()[1]) - float(line.split()[0])
            elif 'ylo' in line and 'yhi' in line:
                unitcell[1, 1] = float(line.split()[1]) - float(line.split()[0])
            elif 'zlo' in line and 'zhi' in line:
                unitcell[2, 2] = float(line.split()[1]) - float(line.split()[0])
            elif 'xy' in line and 'xz' in line and 'yz' in line:
                unitcell[1, 0] = float(line.split()[0])
                unitcell[2, 0] = float(line.split()[1])
                unitcell[2, 1] = float(line.split()[2])
            elif 'Masses' in line:
                self.type_element_symbols = [element.get_element_symbol_by_mass(float(x.split()[1])) for x in file_content[i + 2: i + 2 + n_atom_types]]
            elif 'Pair Coeffs' in line:
                self.pair_coefficients = np.array( [np.fromstring(x.split('#')[0], sep=' ') for x in file_content[i + 2: i + 2 + n_atom_types]] )[:, 1:]
            elif 'Bond Coeffs' in line:
                self.bond_coefficients = np.array( [np.fromstring(x.split('#')[0], sep=' ') for x in file_content[i + 2: i + 2 + n_bond_types]] )[:, 1:]
            elif 'Angle Coeffs' in line:
                self.angle_coefficients = np.array( [np.fromstring(x.split('#')[0], sep=' ') for x in file_content[i + 2: i + 2 + n_angle_types]] )[:, 1:]
            elif 'Dihedral Coeffs' in line:
                self.dihedral_angle_coefficients = np.array( [np.fromstring(x.split('#')[0], sep=' ') for x in file_content[i + 2: i + 2 + n_dihedral_types]] )[:, 1:]
            elif 'Improper Coeffs' in line:
                self.improper_dihedral_angle_coefficients = np.array( [np.fromstring(x.split('#')[0], sep=' ') for x in file_content[i + 2: i + 2 + n_improper_types]] )[:, 1:]
            elif 'Atoms' in line:
                matrix = np.array([np.fromstring(x, sep=' ') for x in file_content[i + 2: i + 2 + n_atoms]])
                if data_format == 'atomic':
                    atomic_numbers = [element.get_atomic_number(self.type_element_symbols[int(x) - 1]) for x in matrix[:, 1]]
                    self.atom_types = list(map(int, matrix[:, 1]))
                    self.atomic_positions_cartesian = np.column_stack((matrix[:, -3:], atomic_numbers))
                elif data_format == 'charge':
                    atomic_numbers = [element.get_atomic_number(self.type_element_symbols[int(x) - 1]) for x in matrix[:, 1]]
                    self.atom_types = list(map(int, matrix[:, 1]))
                    self.atomic_positions_cartesian = np.column_stack((matrix[:, -3:], atomic_numbers))
                    self.charges = matrix[:, 2]
                elif data_format == 'full':
                    atomic_numbers = [element.get_atomic_number(self.type_element_symbols[int(x) - 1]) for x in matrix[:, 2]]
                    self.atom_types = list(map(int, matrix[:, 2]))
                    self.atomic_positions_cartesian = np.column_stack((matrix[:, -3:], atomic_numbers))
                    self.charges = matrix[:, 3]
                    self.mol_ids = list(map(int, matrix[:, 1]))
                    if len(self.fragments) == 0:
                        self.fragments = [[] for x in range(max(self.mol_ids) - min(self.mol_ids) + 1)]
                        for j in range(n_atoms):
                            self.fragments[self.mol_ids[j] - min(self.mol_ids)].append(j)
                else:
                    raise TypeError('Not supported lammps data format ' + data_format)
                self.cartesian_to_fractional()
            elif 'Bonds' in line:
                self.bonds = np.array( [np.fromstring(x, dtype=int, sep=' ') for x in file_content[i + 2: i + 2 + n_bonds]] )[:, 1:]
            elif 'Angles' in line:
                self.angles = np.array( [np.fromstring(x, dtype=int, sep=' ') for x in file_content[i + 2: i + 2 + n_angles]] )[:, 1:]
            elif 'Dihedrals' in line:
                self.dihedral_angles = np.array( [np.fromstring(x, dtype=int, sep=' ') for x in file_content[i + 2: i + 2 + n_dihedrals]] )[:, 1:]
            elif 'Impropers' in line:
                self.improper_dihedral_angles = np.array( [np.fromstring(x, dtype=int, sep=' ') for x in file_content[i + 2: i + 2 + n_impropers]] )[:, 1:]
        if np.fabs(unitcell[0, 0]) < 1e-6 or np.fabs(unitcell[1, 1]) < 1e-6 or np.fabs(unitcell[2, 2]) < 1e-6:
            raise ValueError('The unitcell is not correct')
        else:
            self.unitcell = unitcell
            self.cartesian_to_fractional()
        print('done')

    @property
    def type_element_symbols(self):
        return self.__type_element_symbols

    @type_element_symbols.setter
    def type_element_symbols(self, value):
        self.__type_element_symbols = value

    def write(self, filename, data_format='full'):
        """
            write lammps input data
            arguments:
                filename: output file name; str
                data_format: lammps data format; can be 'atomic', 'charge', 'full', defaults to 'full'
        """
        if (data_format == 'charge' or data_format == 'full' ) and len(self.charges) == 0:
            raise ValueError('No charges available')
        if data_format == 'full' and len(self.mol_ids) == 0:
            raise ValueError('No molecule ids available')

        try:
            fileout = open(filename, 'w', encoding='utf-8')
        except IOError:
            raise
        print('writing ' + filename + ' ... ', end='')

        if self.name:
            fileout.write(self.name)
        fileout.write('\n')

        fileout.write(str(self.number_of_atoms) + ' atoms\n')
        if len(self.bonds) > 0:
            fileout.write(str(len(self.bonds)) + ' bonds\n')
        if len(self.angles) > 0:
            fileout.write(str(len(self.angles)) + ' angles\n')
        if len(self.dihedral_angles) > 0:
            fileout.write(str(len(self.dihedral_angles)) + ' dihedrals\n')
        if len(self.improper_dihedral_angles) > 0:
            fileout.write(str(len(self.improper_dihedral_angles)) + ' impropers\n')
        fileout.write('\n')

        if len(self.pair_coefficients) > 0:
            fileout.write(str(len(self.pair_coefficients)) + ' atom types\n')
        else:
            fileout.write(str(len(self.unique_elements)) + ' atom types\n') 
        if len(self.bond_coefficients) > 0:
            fileout.write(str(len(self.bond_coefficients)) + ' bond types\n')
        if len(self.angle_coefficients) > 0:
            fileout.write(str(len(self.angle_coefficients)) + ' angle types\n')
        if len(self.dihedral_angle_coefficients) > 0:
            fileout.write(str(len(self.dihedral_angle_coefficients)) + ' dihedral types\n')
        if len(self.improper_dihedral_angle_coefficients) > 0:
            fileout.write(str(len(self.improper_dihedral_angle_coefficients)) + ' improper types\n')
        fileout.write('\n')

        cell = copy.deepcopy(self.unitcell)
        np.tril(cell)
        fileout.write('0 ' + str(np.linalg.norm(cell[0, 0])) + ' xlo xhi\n')
        fileout.write('0 ' + str(np.linalg.norm(cell[1, 1])) + ' ylo yhi\n')
        fileout.write('0 ' + str(np.linalg.norm(cell[2, 2])) + ' zlo zhi\n')
        if np.fabs(cell[1, 0]) > 1e-6 or np.fabs(cell[2, 0]) > 1e-6 or np.fabs(cell[2, 1]) > 1e-6:
            fileout.write('{0:f} {1:f} {2:f} xy xz yz\n'.format(cell[1,0], cell[2, 0], cell[2, 1]))
        fileout.write('\n')

        fileout.write('Masses\n# id mass\n')
        if len(self.type_element_symbols) > 0:
            for i, s in enumerate(self.type_element_symbols):
                fileout.write('{0:d} {1:f} # {2:s}\n'.format(i + 1, element.get_mass(s), s))
        else:
            for i, m in enumerate(self.unique_elements):
                fileout.write('{0:d} {1:f} # {2:s}\n'.format(i + 1, element.get_mass(m), m))
        fileout.write('\n')
        
        if len(self.pair_coefficients) > 0:
            fileout.write('Pair Coeffs\n# id epsilon sigma\n')
            for i, c in enumerate(self.pair_coefficients):
                fileout.write(str(i + 1))
                for j in c:
                    fileout.write(' ' + str(j))
                fileout.write('\n')
            fileout.write('\n')

        if len(self.bond_coefficients) > 0:
            fileout.write('Bond Coeffs\n\n')
            for i, c in enumerate(self.bond_coefficients):
                fileout.write(str(i + 1))
                for j in c:
                    fileout.write(' ' + str(j))
                fileout.write('\n')
            fileout.write('\n')
        
        if len(self.angle_coefficients) > 0:
            fileout.write('Angle Coeffs\n\n')
            for i, c in enumerate(self.angle_coefficients):
                fileout.write(str(i + 1))
                for j in c:
                    fileout.write(' ' + str(j))
                fileout.write('\n')
            fileout.write('\n')

        if len(self.dihedral_angle_coefficients) > 0:
            fileout.write('Dihedral Coeffs\n\n')
            for i, c in enumerate(self.dihedral_angle_coefficients):
                fileout.write(str(i + 1))
                # charmm style, second and third parameter must be integer
                for j, k in enumerate(c):
                    if j >= 1 and j <= 2:
                        fileout.write(' ' + str(int(k)))
                    else:
                        fileout.write(' ' + str(k))
                fileout.write('\n')
            fileout.write('\n')

        if len(self.improper_dihedral_angle_coefficients) > 0:
            fileout.write('Improper Coeffs\n\n')
            for i, c in enumerate(self.improper_dihedral_angle_coefficients):
                fileout.write(str(i + 1))
                # cvvf style, second and third parameter must be integer
                for j, k in enumerate(c):
                    if j >= 1 and j <= 2:
                        fileout.write(' ' + str(int(k)))
                    else:
                        fileout.write(' ' + str(k))
                fileout.write('\n')
            fileout.write('\n')

        fileout.write('Atoms\n')
        if data_format == 'atomic':
            fileout.write('# id type x y z\n')
        elif data_format == 'charge':
            fileout.write('# id type q x y z\n')
        elif data_format == 'full':
            fileout.write('# id mol-id type q x y z\n')
        if len(self.atom_types) == 0:
            # use unique_elements to popluate the atom_types
            [self.unique_elements.index(element.get_element_symbol( i[3] )) + 1 for i in self.atomic_positions_cartesian]
        for i, p in enumerate(self.atomic_positions_cartesian):
            if data_format == 'atomic':
                fileout.write('{0:d} {1:d} {2:f} {3:f} {4:f}\n'.format(i + 1, self.atom_types[i], p[0], p[1], p[2]))
            elif data_format == 'charge':
                fileout.write('{0:d} {1:d} {2:f} {3:f} {4:f} {5:f}\n'.format(i + 1, self.atom_types[i], self.charges[i], p[0], p[1], p[2]))
            elif data_format == 'full':
                fileout.write('{0:d} {1:d} {2:d} {3:f} {4:f} {5:f} {6:f}\n'.format(i + 1, self.mol_ids[i], self.atom_types[i], self.charges[i], p[0], p[1], p[2]))
        fileout.write('\n')

        if len(self.bonds) > 0:
            fileout.write('Bonds\n# id type atom1 atom2\n')
            for i, b in enumerate(self.bonds):
                fileout.write('{0:d} {1:d} {2:d} {3:d}\n'.format(i + 1, b[0], b[1], b[2]))
            fileout.write('\n')

        if len(self.angles) > 0:
            fileout.write('Angles\n# id type atom1 atom2 atom3\n')
            for i, a in enumerate(self.angles):
                fileout.write('{0:d} {1:d} {2:d} {3:d} {4:d}\n'.format(i + 1, a[0], a[1], a[2], a[3]))
            fileout.write('\n')

        if len(self.dihedral_angles) > 0:
            fileout.write('Dihedrals\n# id type atom1 atom2 atom3 atom4\n')
            for i, d in enumerate(self.dihedral_angles):
                fileout.write('{0:d} {1:d} {2:d} {3:d} {4:d} {5:d}\n'.format(i + 1, d[0], d[1], d[2], d[3], d[4]))
            fileout.write('\n')

        if len(self.improper_dihedral_angles) > 0:
            fileout.write('Impropers\n# id type atom1 atom2 atom3 atom4\n')
            for i, d in enumerate(self.improper_dihedral_angles):
                fileout.write('{0:d} {1:d} {2:d} {3:d} {4:d} {5:d}\n'.format(i + 1, d[0], d[1], d[2], d[3], d[4]))
            fileout.write('\n')

        fileout.close()
        print('done')

