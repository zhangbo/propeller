#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Bo Zhang

class AMBER:
    """
        AMBER style force field
        members:
            __angle_parameters: angle parameters; [I, J, K, TK, TEQ]: I, J, K are atom types forming angle I-J-K, J is the central atom, TK is the harmonic force constant (kcal/mol/rad^2), TEQ is the equilibrium angle in degree
            __atom_types: atom types; 1 or 2 chars
            __bond_parameters: bond parameters; [I, J, RK, REQ]: I, J are atom types forming bond, RK is the harmonic force constant (kcal/mol/Angstrom^2), REQ is the equilibrium bond length in Angstrom
            __dihedral_angle_parameters: dihedral angle parameters; [I, J, K, L, IDIVF, PK, PHASE, PN]: I, J, K, L are atom types forming dihedral angle, IDIVF is the factor by which the torsional barrier is divided, PK is the barrier height divided by 2, PHASE is the shift angle in degree, PN is periodicity of the barrier; the actual torsional potential will be (PK/IDIVF) * (1 + cos(PN*phi - PHASE))
            __improper_angle_parameters: improper angle parameters; same as __dihedral_angle_parameters, note K is the central atom here
            __masses: mass of each atom type; [I, M]: I is the atom type, M is the mass
            __vdw_parameters: vdW parameters; [I, A, C] if __vdw_style == 'AC': I is the atom type, A is the coefficient of the 12th power term (A/r^12), C is the coefficient of the 6th power term (-C/r^6) or [I, R, EDEP] if __vdw_style == 'RE': I is the atom type, R is the vdW radius in Angstrom, EDEP is the potential well depth in kcal/mol
            __vdw_style: vdW parameter style; can only 'RE' or 'AC'
    """
    def __init__(self):
        """ initialization """
        self.__angle_parameters = []
        self.__atom_types = []
        self.__bond_parameters = []
        self.__dihedral_angle_parameters = []
        self.__improper_angle_parameters = []
        self.__masses = []
        self.__vdw_parameters = []
        self.__vdw_style = 'RE'

    @property
    def angle_parameters(self):
        return self.__angle_parameters

    @angle_parameters.setter
    def angle_parameters(self, value):
        self.__angle_parameters = value

    @property
    def atom_types(self):
        return self.__atom_types

    @atom_types.setter
    def atom_types(self, value):
        self.__atom_types = value

    @property
    def bond_parameters(self):
        return self.__bond_parameters

    @bond_parameters.setter
    def bond_parameters(self, value):
        self.__bond_parameters = value

    @property
    def dihedral_angle_parameters(self):
        return self.__dihedral_angle_parameters

    @dihedral_angle_parameters.setter
    def dihedral_angle_parameters(self, value):
        self.__dihedral_angle_parameters = value

    @property
    def improper_angle_parameters(self):
        return self.__improper_angle_parameters

    @improper_angle_parameters.setter
    def improper_angle_parameters(self, value):
        self.__improper_angle_parameters = value

    @property
    def masses(self):
        return self.__masses

    @masses.setter
    def masses(self, value):
        self.__masses = value

    def read(self, filename, vdw_style='RE'):
        """
            read AMBER force field file
            arguments:
                filename: input file name; str
                vdw_style: style for vdw parameters; can only be 'RE' or 'AC'
        """
        try:
            filein = open(filename, 'r', encoding='utf-8')
        except IOError:
            raise
        print('reading ' + filename + ' ... ', end='')
        file_content = filein.readlines()
        filein.close()
        for i, line in enumerate(file_content):
            if line.strip() == 'MASS':
                j = i + 1
                while len(file_content[j].strip()) > 0:
                    j += 1
                self.atom_types = [x.split()[0] for x in file_content[i + 1: j]]
                self.masses = [[x.split()[0], float(x.split()[1])] for x in file_content[i + 1: j]]
            elif line.strip() == 'BOND':
                j = i + 1
                while len(file_content[j].strip()) > 0:
                    j += 1
                self.bond_parameters = [x.split()[:2] + [float(x.split()[2]), float(x.split()[3])] for x in file_content[i + 1: j]]
            elif line.strip() == 'ANGL':
                j = i + 1
                while len(file_content[j].strip()) > 0:
                    j += 1
                self.angle_parameters = [x.split()[:3] + [float(x.split()[3]), float(x.split()[4])] for x in file_content[i + 1: j]]
            elif line.strip() == 'DIHE':
                j = i + 1
                while len(file_content[j].strip()) > 0:
                    j += 1
                self.dihedral_angle_parameters = [x.split()[:4] + [int(x.split()[4]), float(x.split()[5]), float(x.split()[6]), float(x.split()[7])] for x in file_content[i + 1: j]]
            elif line.strip() == 'IMPR':
                j = i + 1
                while len(file_content[j].strip()) > 0:
                    j += 1
                self.improper_angle_parameters = [x.split()[:4] + [int(x.split()[4]), float(x.split()[5]), float(x.split()[6]), float(x.split()[7])] for x in file_content[i + 1: j]]
            elif line.strip() == 'NONB':
                self.vdw_style = vdw_style
                j = i + 1
                while len(file_content[j].strip()) > 0:
                    j += 1
                self.vdw_parameters = [[x.split()[0], float(x.split()[1]), float(x.split()[2])] for x in file_content[i + 1: j]]
        print('done')

    @property
    def vdw_parameters(self):
        return self.__vdw_parameters

    @vdw_parameters.setter
    def vdw_parameters(self, value):
        self.__vdw_parameters = value

    @property
    def vdw_style(self):
        return self.__vdw_style

    @vdw_style.setter
    def vdw_style(self, value):
        self.__vdw_style = value

    def write(self, filename, description=''):
        """
            write AMBER force field
            arguments:
                filename: output file name; str
                description: description of the file
        """
        try:
            fileout = open(filename, 'w', encoding='utf-8')
        except IOError:
            raise
        print('writing ' + filename + ' ... ', end='')
        fileout.write(description + '\n')
        fileout.write('# vdw style = ' + self.vdw_style + '\n')
        fileout.write('MASS\n')
        for i in self.masses:
            fileout.write('{0:2s}  {1:f}\n'.format(i[0], i[1]))
        fileout.write('\n')

        fileout.write('BOND\n')
        for i in self.bond_parameters:
            fileout.write('{0:2s}  {1:2s}  {2:f}  {3:f}\n'.format(i[0], i[1], i[2], i[3]))
        fileout.write('\n')

        fileout.write('ANGL\n')
        for i in self.angle_parameters:
            fileout.write('{0:2s}  {1:2s}  {2:2s}  {3:f}  {4:f}\n'.format(i[0], i[1], i[2], i[3], i[4]))
        fileout.write('\n')

        fileout.write('DIHE\n')
        for i in self.dihedral_angle_parameters:
            fileout.write('{0:2s}  {1:2s}  {2:2s}  {3:2s}  {4:d}  {5:f}  {6:f}  {7:f}\n'.format(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7]))
        fileout.write('\n')

        fileout.write('IMPR\n')
        for i in self.improper_angle_parameters:
            fileout.write('{0:2s}  {1:2s}  {2:2s}  {3:2s}  {4:d}  {5:f}  {6:f}  {7:f}\n'.format(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7]))
        fileout.write('\n')
       
        fileout.write('NONB\n')
        for i in self.vdw_parameters:
            fileout.write('{0:2s}  {1:f}  {2:f}\n'.format(i[0], i[1], i[2]))
        fileout.write('\n')
        fileout.close()
        print('done')
