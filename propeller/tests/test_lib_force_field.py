#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Bo Zhang

import os
import sys
parent_directory = os.path.realpath(os.path.abspath('/'.join([os.pardir, os.pardir])))
if parent_directory not in sys.path:
    sys.path.insert(0, parent_directory)
from propeller.lib.force_field import AMBER
import unittest

class ForceFieldUnitTest(unittest.TestCase):
    def setUp(self):
        self.__input_dir = 'test_cases/force field/'
        self.__output_dir = 'output/'
        self.__ref_dir = 'test_cases/force field/reference/'

    def test_force_filed_amber_read_write(self):
        frcmod = AMBER()
        frcmod.read(os.path.join(self.__input_dir, 'amber-co2.frcmod'))
        frcmod.write(os.path.join(self.__output_dir + 'amber-co2-2.frcmod'), description='Amber force field from J. Phys. Chem. B 2007, 111, 7078-7084') 
        f1 = open(os.path.join(self.__output_dir + 'amber-co2-2.frcmod'), 'r', encoding='utf-8')
        f2 = open(os.path.join(self.__ref_dir + 'amber-co2-2.frcmod'), 'r', encoding='utf-8')
        self.assertEqual(f1.readlines(), f2.readlines())
        f1.close()
        f2.close()

if __name__ == '__main__':
    unittest.main()
