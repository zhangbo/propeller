#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Bo Zhang

import os
import numpy as np
import random
import sys
parent_directory = os.path.realpath(os.path.abspath('/'.join([os.pardir, os.pardir])))
if parent_directory not in sys.path:
    sys.path.insert(0, parent_directory)
import propeller.core.element as element
import unittest

element_table = element.__element_table
element_jmol_colors = element.__jmol_colors
class ElementUnitTest(unittest.TestCase):
    def setUp(self):
        ele = [k for k, v in element_table.items() if v[0] < 90 and v[0] > 0]
        random.shuffle(ele)
        self.atoms = ele[:10]
        self.atomic_numbers = [element_table[i][0] for i in self.atoms]
        self.mass = [element_table[i][4] for i in self.atoms]
        self.covalent_radius = [element_table[i][1] for i in self.atoms]
        self.vdw_radius = [element_table[i][2] for i in self.atoms]
        self.jmol_colors = [element_jmol_colors[i] for i in self.atoms]

    def test_get_atomic_number(self):
        for a, n in zip(self.atoms, self.atomic_numbers):
            self.assertEqual(element.get_atomic_number(a), n)

    def test_get_bond_length(self):
        np.testing.assert_almost_equal(element.get_bond_length('Ni', 8.0), [1.9, 1.675, 1.67, 1.654, 1.75, 1.78])
        np.testing.assert_almost_equal(element.get_bond_length('Ni', 'O', 'covalent_radii'), [1.9])
        np.testing.assert_almost_equal(element.get_bond_length('Ni', 'O', 'bond_valence_table'), [1.675, 1.67, 1.654, 1.75, 1.78])
        np.testing.assert_almost_equal(element.get_bond_length('Pt', 'Pd'), [2.75])
        np.testing.assert_almost_equal(element.get_bond_length(22, 8, 'bv_table_and_covalent'), [2.26, 1.791, 1.815, 1.78, 1.79]) # Ti-O
        np.testing.assert_almost_equal(element.get_bond_length('Ti', 8, 'bond_valence_table'), [1.791, 1.815, 1.78, 1.79]) # Ti-O
        np.testing.assert_almost_equal(element.get_bond_length(22.0, 'O', 'covalent_radii'), [2.26]) # Ti-O
        np.testing.assert_almost_equal(element.get_bond_length('Ag', 79, 'bond_valence_table'), []) # Ag-Au

    def test_get_bond_length_bv_table(self):
        self.assertEqual(element.get_bond_length_bv_table('Ni', 'O'), [1.675, 1.67, 1.654, 1.75, 1.78])
        self.assertEqual(element.get_bond_length_bv_table('Pt', 'Pd'), [])
        self.assertEqual(element.get_bond_length_bv_table(22, 8), [1.791, 1.815, 1.78, 1.79]) # Ti-O
        self.assertEqual(element.get_bond_length_bv_table('Ag', 79), []) # Ag-Au

    def test_get_bond_length_covalent(self):
        np.testing.assert_almost_equal(element.get_bond_length_covalent('Ni', 'O'), [1.9])
        np.testing.assert_almost_equal(element.get_bond_length_covalent('Pt', 'Pd'), [2.75])
        np.testing.assert_almost_equal(element.get_bond_length_covalent(22, 8), [2.26]) # Ti-O
        np.testing.assert_almost_equal(element.get_bond_length_covalent('Ag', 79), [2.81]) # Ag-Au

    def test_get_covalent_radii(self):
        for a, cr in zip(self.atoms, self.covalent_radius):
            self.assertEqual(element.get_covalent_radii(a), cr)
        for n, cr in zip(self.atomic_numbers, self.covalent_radius):
            self.assertEqual(element.get_covalent_radii(n), cr)

    def test_get_element_symbol(self):
        for n, a in zip(self.atomic_numbers, self.atoms):
            self.assertEqual(element.get_element_symbol(n), a)

    def test_get_element_symbol_by_mass(self):
        for m, a in zip(self.mass, self.atoms):
            self.assertEqual(element.get_element_symbol_by_mass(m), a)

    def test_get_group_number(self):
        a = ['H', 2.0, 'Li', 6, 'Na', 18, 'K', 22.0, 'Pd', 'Ba', 'Ce', 78.0, 'Ra', 'U', 111]
        g = [1, 18, 1, 14, 1, 18, 1, 4, 10, 2, 3, 10, 2, 3, 11]
        for i, j in zip(a, g):
            self.assertEqual(element.get_group_number(i), j)

    def test_get_mass(self):
        for a, m in zip(self.atoms, self.mass):
            self.assertEqual(element.get_mass(a), m)
        for n, m in zip(self.atomic_numbers, self.mass):
            self.assertEqual(element.get_mass(n), m)

    def test_get_number_of_valence_electron(self):
        a = ['H', 2.0, 'Li', 6, 'Na', 18, 'K', 22.0, 'Pd', 'Ba', 'Ce', 78.0, 'Ra', 'U', 111]
        g = [1, 8, 1, 4, 1, 8, 1, 4, 10, 2, 3, 10, 2, 3, 11]
        for i, j in zip(a, g):
            self.assertEqual(element.get_number_of_valence_electron(i), j)

    def test_get_vdw_radii(self):
        for a, vr in zip(self.atoms, self.vdw_radius):
            self.assertEqual(element.get_vdw_radii(a), vr)
        for n, vr in zip(self.atomic_numbers, self.vdw_radius):
            self.assertEqual(element.get_vdw_radii(n), vr)

if __name__ == '__main__':
    unittest.main()
