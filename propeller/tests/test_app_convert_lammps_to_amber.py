#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Bo Zhang

import os
import numpy as np
import sys
parent_directory = os.path.realpath(os.path.abspath('/'.join([os.pardir, os.pardir])))
if parent_directory not in sys.path:
    sys.path.insert(0, parent_directory)
import propeller.app.convert_lammps_to_amber as lammps2amber
import unittest

class ConvertLammpsToAMBERUnitTest(unittest.TestCase):
    def setUp(self):
        self.__input_dir = 'test_cases/convert_lammps_to_amber/'
        self.__output_dir = 'output/'
        self.__ref_dir = 'test_cases/convert_lammps_to_amber/reference/'

    def test_convert_lammps_to_amber(self):
        l2a = lammps2amber.ConvertLammpsToAMBER()
        l2a.read(os.path.join(self.__input_dir, 'data.bmim-tf2n'))
        l2a.convert()
        l2a.write(os.path.join(self.__output_dir, 'bmim-tf2n.xyz'), os.path.join(self.__output_dir, 'bmim-tf2n.frcmod'), os.path.join(self.__output_dir, 'bmim-tf2n-atc.dat'))

        f1 = open(os.path.join(self.__output_dir, 'bmim-tf2n.xyz'), 'r', encoding='utf-8')
        f2 = open(os.path.join(self.__ref_dir, 'bmim-tf2n-ref.xyz'), 'r', encoding='utf-8')
        self.assertEqual(f1.readlines(), f2.readlines())
        f1.close()
        f2.close()
        f3 = open(os.path.join(self.__output_dir, 'bmim-tf2n.frcmod'), 'r', encoding='utf-8')
        f4 = open(os.path.join(self.__ref_dir, 'bmim-tf2n-ref.frcmod'), 'r', encoding='utf-8')
        self.assertEqual(f3.readlines(), f4.readlines())
        f3.close()
        f4.close()
        f5 = open(os.path.join(self.__output_dir, 'bmim-tf2n-atc.dat'), 'r', encoding='utf-8')
        f6 = open(os.path.join(self.__ref_dir, 'bmim-tf2n-atc-ref.dat'), 'r', encoding='utf-8')
        self.assertEqual(f5.readlines(), f6.readlines())
        f5.close()
        f6.close()
        
        l2a2 = lammps2amber.ConvertLammpsToAMBER()
        l2a2.read(os.path.join(self.__input_dir, 'data.bmim-tf2n-co2'))
        l2a2.convert()
        l2a2.write(os.path.join(self.__output_dir, 'bmim-tf2n-co2.xyz'), os.path.join(self.__output_dir, 'bmim-tf2n-co2.frcmod'), os.path.join(self.__output_dir, 'bmim-tf2n-co2-atc.dat'))

        f7 = open(os.path.join(self.__output_dir, 'bmim-tf2n-co2.xyz'), 'r', encoding='utf-8')
        f8 = open(os.path.join(self.__ref_dir, 'bmim-tf2n-co2-ref.xyz'), 'r', encoding='utf-8')
        self.assertEqual(f7.readlines(), f8.readlines())
        f7.close()
        f8.close()
        f9 = open(os.path.join(self.__output_dir, 'bmim-tf2n-co2.frcmod'), 'r', encoding='utf-8')
        f10 = open(os.path.join(self.__ref_dir, 'bmim-tf2n-co2-ref.frcmod'), 'r', encoding='utf-8')
        self.assertEqual(f9.readlines(), f10.readlines())
        f9.close()
        f10.close()
        f11 = open(os.path.join(self.__output_dir, 'bmim-tf2n-co2-atc.dat'), 'r', encoding='utf-8')
        f12 = open(os.path.join(self.__ref_dir, 'bmim-tf2n-co2-atc-ref.dat'), 'r', encoding='utf-8')
        self.assertEqual(f11.readlines(), f12.readlines())
        f11.close()
        f12.close()
if __name__ == '__main__':
    unittest.main()
