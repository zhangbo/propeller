#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Bo Zhang

import os
import numpy as np
import sys
parent_directory = os.path.realpath(os.path.abspath('/'.join([os.pardir, os.pardir])))
if parent_directory not in sys.path:
    sys.path.insert(0, parent_directory)
import propeller.app.convert_amber_to_lammps as amber2lammps
import unittest

class ConvertAMBERToLammpsSUnitTest(unittest.TestCase):
    def setUp(self):
        self.__input_dir = 'test_cases/convert_amber_to_lammps/'
        self.__output_dir = 'output/'
        self.__ref_dir = 'test_cases/convert_amber_to_lammps/reference/'

    def test_convert_amber_to_lammps(self):
        a2l = amber2lammps.ConvertAMBERToLammps()
        a2l.read(os.path.join(self.__input_dir, 'bmim-tf2n.xyz'), os.path.join(self.__input_dir, 'bmim-tf2n.frcmod'), os.path.join(self.__input_dir, 'bmim-tf2n-atc.dat'))
        a2l.convert([44, 44, 161], bonding_tolerance=0.22, is_verbose=True)
        a2l.write(os.path.join(self.__output_dir, 'data.bmim-tf2n'))

        f1 = open(os.path.join(self.__output_dir, 'data.bmim-tf2n'), 'r', encoding='utf-8')
        f2 = open(os.path.join(self.__ref_dir, 'data.bmim-tf2n-ref'), 'r', encoding='utf-8')
        self.assertEqual(f1.readlines(), f2.readlines())
        f1.close()
        f2.close()

        a2l = amber2lammps.ConvertAMBERToLammps()
        a2l.read(os.path.join(self.__input_dir, 'pc44gly.xyz'), os.path.join(self.__input_dir, 'pc44gly.frcmod'), os.path.join(self.__input_dir, 'pc44gly-co2-atom-type-charge.dat'))
        a2l.convert([8.316, 8.316, 8.316], bonding_tolerance=0.20, is_verbose=True)
        a2l.write(os.path.join(self.__output_dir, 'data.pc44gly'))

        f1 = open(os.path.join(self.__output_dir, 'data.pc44gly'), 'r', encoding='utf-8')
        f2 = open(os.path.join(self.__ref_dir, 'data.pc44gly-ref'), 'r', encoding='utf-8')
        self.assertEqual(f1.readlines(), f2.readlines())
        f1.close()
        f2.close()
        
        a2l = amber2lammps.ConvertAMBERToLammps()
        a2l.read(os.path.join(self.__input_dir, '50pg-50co2.xyz'), os.path.join(self.__input_dir, 'pc44gly-co2.frcmod'), os.path.join(self.__input_dir, 'pc44gly-co2-atom-type-charge.dat'))
        a2l.convert([52, 52, 52], bonding_tolerance=0.22, is_verbose=True)
        a2l.write(os.path.join(self.__output_dir, 'data.50pg-50co2'))

        f1 = open(os.path.join(self.__output_dir, 'data.50pg-50co2'), 'r', encoding='utf-8')
        f2 = open(os.path.join(self.__ref_dir, 'data.50pg-50co2-ref'), 'r', encoding='utf-8')
        for l1, l2 in zip(f1.readlines(), f2.readlines()):
            self.assertEqual(l1, l2)
        #self.assertEqual(f1.readlines(), f2.readlines())
        f1.close()
        f2.close()

if __name__ == '__main__':
    unittest.main()
