#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Bo Zhang

import os
import numpy as np
import random
import sys
parent_directory = os.path.realpath(os.path.abspath('/'.join([os.pardir, os.pardir])))
if parent_directory not in sys.path:
    sys.path.insert(0, parent_directory)
import propeller.core.molecule as molecule
import propeller.lib.lammps as lammps
import unittest

class LammpsUnitTest(unittest.TestCase):
    def setUp(self):
        self.__input_dir = 'test_cases/lammps/'
        self.__output_dir = 'output/'
        self.__ref_dir = 'test_cases/lammps/reference/'

    def test_lammps_data_read_write(self):
        lmp = lammps.Data()
        lmp.read(os.path.join(self.__input_dir, 'data.50co2-50il-8000000'), data_format='full')
        lmp.write(os.path.join(self.__output_dir, 'data.50co2-50il-8000000-1'), data_format='full')
        f1 = open(os.path.join(self.__output_dir, 'data.50co2-50il-8000000-1'), 'r', encoding='utf-8')
        f2 = open(os.path.join(self.__ref_dir, 'data.50co2-50il-8000000-ref'), 'r', encoding='utf-8')
        self.assertEqual(f1.readlines(), f2.readlines())
        f1.close()
        f2.close()

        lmp = lammps.Data()
        lmp.read(os.path.join(self.__input_dir, 'data.nve-last'), data_format='full')
        lmp.write(os.path.join(self.__output_dir, 'data.nve-last-1'), data_format='full')
        f1 = open(os.path.join(self.__output_dir, 'data.nve-last-1'), 'r', encoding='utf-8')
        f2 = open(os.path.join(self.__ref_dir, 'data.nve-last-ref'), 'r', encoding='utf-8')
        self.assertEqual(f1.readlines(), f2.readlines())
        f1.close()
        f2.close()
        fragment1 = [62, 63, 65, 66, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 90, 91, 92, 94, 95, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 123]
        self.assertEqual(lmp.fragments[2], fragment1)


if __name__ == '__main__':
    unittest.main()
