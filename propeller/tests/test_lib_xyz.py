#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Bo Zhang

import os
import numpy as np
import sys
parent_directory = os.path.realpath(os.path.abspath('/'.join([os.pardir, os.pardir])))
if parent_directory not in sys.path:
    sys.path.insert(0, parent_directory)
import propeller.lib.xyz as xyz
import unittest

class XyzUnitTest(unittest.TestCase):
    def setUp(self):
        self.__input_dir = 'test_cases/xyz/'
        self.__output_dir = 'output/'
        self.__ref_dir = 'test_cases/xyz/reference/'

    def test_xyz_read_write(self):
        xyz1 = xyz.Xyz()
        xyz1.read(os.path.join(self.__input_dir, 'pgcc-zif6.xyz'))
        xyz1.write(os.path.join(self.__output_dir, 'pgcc-2.xyz'))
        xyz1.write(os.path.join(self.__output_dir, 'pgcc-3.xyz'), is_writing_charge=True)
        f1 = open(os.path.join(self.__output_dir, 'pgcc-2.xyz'), 'r', encoding='utf-8')
        f2 = open(os.path.join(self.__ref_dir, 'pgcc-ref-no-charge.xyz'), 'r', encoding='utf-8')
        self.assertEqual(f1.readlines(), f2.readlines())
        f1.close()
        f2.close()
        f3 = open(os.path.join(self.__output_dir, 'pgcc-3.xyz'), 'r', encoding='utf-8')
        f4 = open(os.path.join(self.__ref_dir, 'pgcc-ref-charge.xyz'), 'r', encoding='utf-8')
        self.assertEqual(f3.readlines(), f4.readlines())
        f3.close()
        f4.close()

    def test_xyzs_read_write(self):
        xyzs = xyz.Xyzs()
        xyzs.read(os.path.join(self.__input_dir, 'pgcc-zif6.xyz'))
        xyzs.write(os.path.join(self.__output_dir, 'pgcc-zif6-4.xyz'))
        xyzs.write(os.path.join(self.__output_dir, 'pgcc-zif6-5.xyz'), is_writing_charge=True)
        f1 = open(os.path.join(self.__output_dir, 'pgcc-zif6-4.xyz'), 'r', encoding='utf-8')
        f2 = open(os.path.join(self.__ref_dir, 'pgcc-zif6-ref-no-charge.xyz'), 'r', encoding='utf-8')
        self.assertEqual(f1.readlines(), f2.readlines())
        f1.close()
        f2.close()
        f3 = open(os.path.join(self.__output_dir, 'pgcc-zif6-5.xyz'), 'r', encoding='utf-8')
        f4 = open(os.path.join(self.__ref_dir, 'pgcc-zif6-ref-charge.xyz'), 'r', encoding='utf-8')
        self.assertEqual(f3.readlines(), f4.readlines())
        f3.close()
        f4.close()

if __name__ == '__main__':
    unittest.main()
