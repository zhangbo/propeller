#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Bo Zhang

import copy
import math
import propeller.core.element as element
import propeller.lib.force_field as force_field
import propeller.lib.lammps as lammps
import propeller.lib.xyz as xyz

class ConvertLammpsToAMBER:
    """
        convert lammps data file to geometry file (xyz) and AMBER style force field file (frcmod)
        members:
            __force_field: force field; class AMBER
            __lammps_data: lammps data file; class Data
    """
    def __init__(self):
        """ initialization """
        self.__force_field = force_field.AMBER()
        self.__lammps_data = lammps.Data()
   
    def convert(self):
        """ convert LAMMPS data file to AMBER force field """
        print('converting ... ', end='')
        for s in self.__lammps_data.type_element_symbols:
            if s not in self.__force_field.atom_types:
                self.__force_field.atom_types.append(s)
            else:
                i = 1
                while s + str(i) in self.__force_field.atom_types:
                    i += 1
                self.__force_field.atom_types.append(s + str(i))

        self.__force_field.masses = [[i, element.get_mass(j)] for i, j in zip(self.__force_field.atom_types, self.__lammps_data.type_element_symbols)]
        self.__force_field.vdw_style = 'RE'
        self.__force_field.vdw_parameters = [[i, j[1], j[0]] for i, j in zip(self.__force_field.atom_types, self.__lammps_data.pair_coefficients)]
        # harmonic
        for i, c in enumerate(self.__lammps_data.bond_coefficients):
            for j in self.__lammps_data.bonds:
                if j[0] == i + 1:
                    self.__force_field.bond_parameters.append([self.__force_field.atom_types[self.__lammps_data.atom_types[j[1] - 1] - 1], self.__force_field.atom_types[self.__lammps_data.atom_types[j[2] - 1] - 1], c[0], c[1]])
                    break

        # harmonic
        for i, c in enumerate(self.__lammps_data.angle_coefficients):
            for j in self.__lammps_data.angles:
                if j[0] == i + 1:
                    # note that the second atom in sequence are central atom in both LAMMPS and AMBER
                    self.__force_field.angle_parameters.append([self.__force_field.atom_types[self.__lammps_data.atom_types[j[1] - 1] - 1], self.__force_field.atom_types[self.__lammps_data.atom_types[j[2] - 1] - 1], self.__force_field.atom_types[self.__lammps_data.atom_types[j[3] - 1] - 1], c[0], c[1]])
                    break

        # charmm style 
        for i, c in enumerate(self.__lammps_data.dihedral_angle_coefficients):
            for j in self.__lammps_data.dihedral_angles:
                if j[0] == i + 1:
                    self.__force_field.dihedral_angle_parameters.append([self.__force_field.atom_types[self.__lammps_data.atom_types[j[1] - 1] - 1], self.__force_field.atom_types[self.__lammps_data.atom_types[j[2] - 1] - 1], self.__force_field.atom_types[self.__lammps_data.atom_types[j[3] - 1] - 1], self.__force_field.atom_types[self.__lammps_data.atom_types[j[4] - 1] - 1], 2, c[0] * 2, c[2], c[1]])
                    break

        # cvff style
        for i, c in enumerate(self.__lammps_data.improper_dihedral_angle_coefficients):
            for j in self.__lammps_data.improper_dihedral_angles:
                if j[0] == i + 1:
                    if math.fabs(math.fabs(c[1]) - 1) > 1e-3:
                        raise ValueError('d must be -1 or 1')
                    # note that the third atom in AMBER is the central atom while the first atom in LAMMPS is the central atom
                    self.__force_field.improper_angle_parameters.append([self.__force_field.atom_types[self.__lammps_data.atom_types[j[2] - 1] - 1], self.__force_field.atom_types[self.__lammps_data.atom_types[j[3] - 1] - 1], self.__force_field.atom_types[self.__lammps_data.atom_types[j[1] - 1] - 1], self.__force_field.atom_types[self.__lammps_data.atom_types[j[4] - 1] - 1], 2, c[0] * 2, math.acos(c[1]) / math.pi * 180, c[2]])
                    break 
        print('done')

    def read(self, filename, pair_style='lj', bond_style='harmonic', angle_style='harmonic', dihedral_style='charmm', improper_style='cvff'):
        """
            read lammps data file
            arguments:
                filename: file name; str
                pair_style: pair coefficient style, can be 'lj'
                bond_style: bond parameters style, can be 'harmonic'
                angle_style: angle parameters style, can be 'harmonic'
                dihedral_style: dihedral angle parameters style, can be 'charmm'
                improper_style: improper dihedral angle parameters style, can be 'cvff'
        """
        pair_style_choices = ['lj']
        bond_style_choices = ['harmonic']
        angle_style_choices = ['harmonic']
        dihedral_style_choices = ['charmm']
        improper_style_choices = ['cvff']
        if pair_style not in pair_style_choices:
            print('WARNING: pair style not in supported list, use with CAUTION')
        if bond_style not in bond_style_choices:
            print('WARNING: bond style not in supported list, use with CAUTION')
        if angle_style not in angle_style_choices:
            print('WARNING: angle style not in supported list, use with CAUTION')
        if dihedral_style not in dihedral_style_choices:
            print('WARNING: dihedral style not in supported list, use with CAUTION')
        if improper_style not in improper_style_choices:
            print('WARNING: improper style not in supported list, use with CAUTION')
        self.__lammps_data.read(filename)

    def write(self, geometry_file, force_field_file, atom_type_charge_file):
        """
            write geometry file, force field file and atom type charge file
            arguments:
                geometry_file: geometry file name; str
                force_field_file: force field file name; str
                atom_type_charge_file: atom type charge file name; str
        """
        geometry_style_list = ['.xyz']
        force_field_style_list = ['.frcmod'] # frcmod: AMBER style
        if not sum(geometry_file.endswith(x) for x in geometry_style_list):
            raise TypeError(geometry_file + ' is not supported')
        if not sum(force_field_file.endswith(x) for x in force_field_style_list):
            raise TypeError(force_field_file + ' is not supported')

        geometry = copy.deepcopy(self.__lammps_data)
        geometry.__class__ = xyz.Xyz
        geometry.write(geometry_file, is_writing_charge=True)
        self.__force_field.write(force_field_file)
        self.write_atom_type_charge(atom_type_charge_file)

    def write_atom_type_charge(self, filename):
        """
            write atom type charge file
            arguments:
                filename: atom type charge file name; str
        """
        try:
            fileout = open(filename, 'w', encoding='utf-8')
        except IOError:
            raise
        print('writing ' + filename + ' ... ', end='')
        fileout.write('format = full\n')
        for fragment in self.__lammps_data.fragments:
            fileout.write('formula: ' + self.__lammps_data.formula(fragment) + '\n')
            fileout.write('# type atom_type charge\n')
            for i, atom_index in enumerate(sorted(fragment)):
                fileout.write(self.__lammps_data.element_symbols[atom_index] + str(i) + ' ' + self.__force_field.atom_types[self.__lammps_data.atom_types[atom_index] - 1] + ' ' + str(self.__lammps_data.charges[atom_index]) + '\n')
        fileout.close()
        print('done')

