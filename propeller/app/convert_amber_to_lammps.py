#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Bo Zhang

import collections
import math
import numpy as np
import propeller.core.element as element
import propeller.lib.force_field as force_field
import propeller.lib.lammps as lammps
import propeller.lib.xyz as xyz
import re

class ConvertAMBERToLammps:
    """
        convert amber force field file (frcmod) and geometry file (xyz) to lammps data file
        members:
            __atom_type_charge: atom type and corresponding charge; dictionary of dictionary {formula: {atom: atom type, charge}}
            __atom_type_charge_style: atom type and corresponding charge style; can be 'full' or 'partial'
            __force_filed: force field; class AMBER
            __geometry: geometry file; class xyz, currently only support xyz file
            __lammps_data: lammps data; class Data
            __primary_atoms: primary atom of a formula, which is designated as zero, every other atom will count from this one; dictionary {formula: atom}
    """
    def __init__(self):
        """ initilization """
        self.__atom_type_charge = collections.OrderedDict()
        self.__atom_type_charge_style = 'full'
        self.__force_filed = force_field.AMBER()
        self.__geometry = xyz.Xyz()
        self.__lammps_data = lammps.Data()
        self.__primary_atoms = collections.OrderedDict()

    def convert(self, lattice_parameters, bonding_criteria='bv_table_and_covalent', bonding_tolerance=0.2, is_verbose=False):
        """ 
            convert geometry file and AMBER force field to LAMMPS data format
            arguments:
                lattice_parameters: lattice parameters; list of float, must be 3-6 numbers
                bonding_criteria: the method to get the bond length of two atoms; str, can only be 'bond_valence_table', 'bv_table_and_covalent', 'covalent_radii', defaults to 'bv_table_and_covalent'
                bonding_tolerance: the allowed difference between the distance of two atoms and the bond length from literature, defaults to 0.2
        """
        print('convert to LAMMPS data ... ', end='')

        if len(lattice_parameters) == 3:
            lattice_parameters.append(90)
            lattice_parameters.append(90)
            lattice_parameters.append(90)
        elif len(lattice_parameters) == 4:
            lattice_parameters.append(90)
            lattice_parameters.append(90)
        elif len(lattice_parameters) == 5:
            lattice_parameters.append(90)
        else:
            raise ValueError('the number of lattice parameters must within 3-6')
        c2 = (math.cos(lattice_parameters[3] * math.pi / 180.0) - math.cos(lattice_parameters[4] * math.pi / 180.0) * math.cos(lattice_parameters[5] * math.pi / 180.0)) / math.sin(lattice_parameters[5] * math.pi / 180.0);
        if math.sin(lattice_parameters[4] * math.pi / 180.0) * math.sin(lattice_parameters[4] * math.pi / 180.0) - c2 * c2 < 0:
            raise ValueError('cannot find square root for negative number')
        c3 = math.sqrt(math.sin(lattice_parameters[4] * math.pi / 180.0) * math.sin(lattice_parameters[4] * math.pi / 180.0) - c2 * c2);
        self.__lammps_data.unitcell=np.array([[lattice_parameters[0], 0, 0], [lattice_parameters[1] * math.cos(lattice_parameters[5] * math.pi / 180.0), lattice_parameters[1] * math.sin(lattice_parameters[5] * math.pi / 180.0), 0], [lattice_parameters[2] * math.cos(lattice_parameters[4] * math.pi / 180.0), lattice_parameters[2] * c2, lattice_parameters[2] * c3]])
        self.__lammps_data.atomic_positions_cartesian = self.__geometry.atomic_positions_cartesian
        self.__lammps_data.type_element_symbols = [element.get_element_symbol_by_mass(i[1]) for i in self.__force_filed.masses] 
        self.__lammps_data.pair_coefficients = len(self.__lammps_data.type_element_symbols) * [[]]
        for p in self.__force_filed.vdw_parameters:
            self.__lammps_data.pair_coefficients[self.__force_filed.atom_types.index(p[0])] = [p[2], p[1]]
        # harmonic potential
        for p in self.__force_filed.bond_parameters:
            self.__lammps_data.bond_coefficients.append([p[2], p[3]])
        # harmonic potential
        for p in self.__force_filed.angle_parameters:
            self.__lammps_data.angle_coefficients.append([p[3], p[4]])
        # charmm style 
        for p in self.__force_filed.dihedral_angle_parameters:
            self.__lammps_data.dihedral_angle_coefficients.append([p[5] / p[4], p[7], p[6], 0])
        # cvff style 
        for p in self.__force_filed.improper_angle_parameters:
            self.__lammps_data.improper_dihedral_angle_coefficients.append([p[5] / p[4], math.cos(p[6] / 180.0 * math.pi), p[7]])
        
        self.__lammps_data.calculate_connectivity(bonding_criteria=bonding_criteria, bonding_tolerance=bonding_tolerance)
        self.__lammps_data.set_fragments_by_connectivity()
        if is_verbose:
            c_counter = collections.Counter((self.__lammps_data.formula(x) for x in self.__lammps_data.fragments))
            print('there are {0:d} species and {1:d} molecules'.format(len(c_counter), len(self.__lammps_data.fragments)))
            print('the chemical formula of species are:')
            for i, j in enumerate(c_counter):
                print(i, j)
        bonds, angles, dihedral_angles, improper_dihedral_angles = self.__lammps_data.find_all_bond_angle_dihedral(is_considering_improper_dihedral=True)
        # assign type and charge
        # first put dummy types, charge, mol_ids
        self.__lammps_data.atom_types = self.__lammps_data.number_of_atoms * [0]
        self.__lammps_data.charges = self.__lammps_data.number_of_atoms * [0]
        self.__lammps_data.mol_ids = self.__lammps_data.number_of_atoms * [0]
        for i, fragment in enumerate(self.__lammps_data.fragments):
            formula = self.__lammps_data.formula(fragment)
            if formula not in self.__atom_type_charge.keys():
                raise KeyError('No formula ' + formula)
            for j, atom_index in enumerate(sorted(fragment)):
                if self.__atom_type_charge_style == 'full':
                    key = self.__lammps_data.element_symbols[atom_index] + str(j)
                elif self.__atom_type_charge_style == 'partial':
                    key = self.__lammps_data.element_symbols[atom_index] + str(self.__lammps_data.find_locant(atom_index, self.__primary_atoms[formula], fragment))
                self.__lammps_data.atom_types[atom_index] = self.__force_filed.atom_types.index( self.__atom_type_charge[formula][key][0] ) + 1
                self.__lammps_data.charges[atom_index] = self.__atom_type_charge[formula][key][1]
                self.__lammps_data.mol_ids[atom_index] = i + 1

        # check if they were assigned correct
        if len([i for i in self.__lammps_data.atom_types if i == 0]) > 0:
            raise ValueError('atom types NOT assigned')
        if len([i for i in self.__lammps_data.charges if i == 0]) > 0:
            raise ValueError('charges NOT assigned')
        if len([i for i in self.__lammps_data.mol_ids if i == 0]) > 0:
            raise ValueError('mol ids NOT assigned')

        # populate bonds, angles, dihedral angles
        for i, b in enumerate(bonds):
            t = self.find_bat_type(b)
            if len(t) == 0:
                raise ValueError('NO bond type found')
            for j in t:
                self.__lammps_data.bonds.append([j, b[0] + 1, b[1] + 1])
        for i, a in enumerate(angles):
            t = self.find_bat_type(a)
            if len(t) == 0:
                raise ValueError('NO angle type found')
            for j in t:
                self.__lammps_data.angles.append([j, a[0] + 1, a[1] + 1, a[2] + 1])
        for i, d in enumerate(dihedral_angles):
            t = self.find_bat_type(d)
            if len(t) == 0:
                print(b)
                raise ValueError('NO dihedral angle type found')
            for j in t:
                self.__lammps_data.dihedral_angles.append([j, d[0] + 1, d[1] + 1, d[2] + 1, d[3] + 1])
        for i, d in enumerate(improper_dihedral_angles):
            t = self.find_bat_type(d, is_improper_dihedral_angle=True)
            if len(t) > 0:
                for j in t:
                    self.__lammps_data.improper_dihedral_angles.append([j, d[0] + 1, d[1] + 1, d[2] + 1, d[3] + 1])
        print('done')

    def find_bat_type(self, bat, is_improper_dihedral_angle=False):
        """
            find bond, angle or dihedral angle type
            arguments:
                bat: bond, angle or dihedral angle; list of int
                is_improper_dihedral_angle: if the input is improper dihedral angle; bool, defaults to False
            returns:
                list of type: list of int; empty means no such type
        """
        atp = [self.__force_filed.atom_types[ self.__lammps_data.atom_types[i] - 1 ] for i in bat]
        bat_types = []
        # bond
        if len(bat) == 2:
            for i, p in enumerate(self.__force_filed.bond_parameters):
                if (atp[0] == p[0] and atp[1] == p[1]) or (atp[0] == p[1] and atp[1] == p[0]):
                    bat_types.append(i + 1)
        # angle 
        if len(bat) == 3:
            for i, p in enumerate(self.__force_filed.angle_parameters):
                if (atp[0] == p[0] and atp[1] == p[1] and atp[2] == p[2]) or (atp[0] == p[2] and atp[1] == p[1] and atp[2] == p[0]):
                    bat_types.append(i + 1)
        if len(bat) == 4:
            # improper dihedral angle 
            if is_improper_dihedral_angle:
                for i, p in enumerate(self.__force_filed.improper_angle_parameters):
                    if atp[0] == p[2] and sorted([atp[1], atp[2], atp[3]]) == sorted([p[0], p[1], p[3]]):
                        bat_types.append(i + 1)
            # dihedral angle 
            else:
                for i, p in enumerate(self.__force_filed.dihedral_angle_parameters):
                    if (atp[0] == p[0] and atp[1] == p[1] and atp[2] == p[2] and atp[3] == p[3]) or (atp[0] == p[3] and atp[1] == p[2] and atp[2] == p[1] and atp[3] == p[0]):
                        bat_types.append(i + 1)
        return bat_types

    def read(self, geometry_file, force_field_file, atom_type_charge_file, force_field_vdw_style='RE'):
        """
            read in geometry, force field, atom_type_charge
            only support 'RE' style AMBER force field, xyz file geometry file
            arguments:
                geometry_file: geometry filename; str
                force_field_file: force field filename; str
                atom_type_charge_file: atom type and corresponding filename; str
                force_field_file_vdw_style: vdw style in force field; currently only support 'RE'
        """
        geometry_style_list = ['.xyz']
        force_field_style_list = ['.frcmod'] # frcmod: AMBER style
        force_field_vdw_style_list = ['RE']
        if not sum(geometry_file.endswith(x) for x in geometry_style_list):
            raise TypeError(geometry_file + ' is not supported')
        if not sum(force_field_file.endswith(x) for x in force_field_style_list):
            raise TypeError(force_field_file + ' is not supported')
        if not sum(force_field_vdw_style.endswith(x) for x in force_field_vdw_style_list):
            raise TypeError('style ' + force_field_vdw_style + ' is not supported')

        self.__geometry.read(geometry_file)
        self.__force_filed.read(force_field_file, vdw_style=force_field_vdw_style)
        self.read_atom_type_charge(atom_type_charge_file)

    def read_atom_type_charge(self, filename):
        """
            read atom type charge file
            arguments:
                filename: file name; str
        """
        print('reading ' + filename + ' ... ', end='')
        try:
            filein = open(filename, 'r', encoding='utf-8')
        except IOError:
            raise
        for line in filein.readlines():
            if line.strip()[0] == '#':
                continue
            if 'format' in line:
                self.__atom_type_charge_style = line.split('=')[1].strip()
            elif 'formula' in line:
                formula = line.split()[1]
                self.__atom_type_charge[formula] = collections.OrderedDict() 
            else:
                # atom : atom_type, charge
                self.__atom_type_charge[formula][line.split()[0]] = [line.split()[1], float(line.split()[2])]
        filein.close()

        if self.__atom_type_charge_style != 'full':
            # we will have to generate type charges later by calculating the locant relative to primary atoms
            for key, value in self.__atom_type_charge.items():
                for k in value.keys():
                    r = re.match('([a-zA-Z]+)([0-9]+)', k)
                    if r and int(r.group(2)) == 0:
                        self.__primary_atoms[key] = r.group(1)
        print('done')

    def write(self, filename, data_format='full'):
        """
            write LAMMPS data file
            arguments:
                filename: output file name; str
                data_format: lammps data format; can be 'atomic', 'charge', 'full', defaults to 'full'
        """
        self.__lammps_data.write(filename, data_format=data_format)

